
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
  backend "s3" {
    bucket = "gitlab-deploy-demo"
    key = "ecr.tfsatte"
    region = "us-east-1"
    #access_key = var.aws_access_key                   #"AKIAYGMP4KUTQSKBUPGS"
    #secret_key = var.aws_secret_access_key            #"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"
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-1"
  access_key = var.aws_access_key
  secret_key = var.aws_secret_access_key
}

resource "aws_ecr_repository" "gitlab-terra" {
  name                 = "gitlab-terra"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true                                         #Scanea la image en cuanto llega al repo
  }
}
/*
data "aws_iam_policy_document" "ecr-data-policy" {
  statement {
    sid    = "AllowPull"
    effect = "Allow"
    #principal = "*"

    principals {
      type        = "AWS"
      identifiers = ["123456789012"]   
    }
 
    actions = [
      "ecr:GetDownloadUrlForLayer",
      "ecr:BatchGetImage",
      "ecr:GetRepositoryPolicy",
    ]
  }
}
*/

resource "aws_ecr_repository_policy" "ecr-repo-policy" {
  repository = aws_ecr_repository.gitlab-terra.name                             #calling el nombre de esta resource (gitlab-terra)
  #policy     = data.aws_iam_policy_document.ecr-data-policy.json

  policy = jsonencode({
      Version = "2008-10-17"
      Statement = [
        {
          Sid       = "AllowPublicPull"
          Effect    = "Allow"
          Principal = "*"
          Action    = [
            "ecr:GetDownloadUrlForLayer",
            "ecr:BatchGetImage",
            "ecr:GetRepositoryPolicy",
          ]
        }
      ]
    })
}

output "repository_url" {
  value = aws_ecr_repository.gitlab-terra.repository_url
}
