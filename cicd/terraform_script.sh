 
 #!  /bin/bash

#login ?
set -e    

##DIRECTORY es una variable creada para usarla para acceder a la carpeta donde esta este script
cd "${DIRECTORY}" || exit                   

#install python
apt-get update && apt-get install -y curl unzip jq python3-pip


#Installing AWS-CLI
pip3 install awscli --break-system-packages

#Setting version of Terraf to install
TERRA_VERSION="1.0.5"

#Download Terraf from la url and save it on terraform.zip file
curl "https://releases.hashicorp.com/terraform/${TERRA_VERSION}/terraform_${TERRA_VERSION}_linux_amd64.zip" -o "terraform.zip"

#Descomprimiendo el .zip file y moverlo a un path y final eliminandolo
unzip terraform.zip && mv terraform /usr/local/bin/
rm terraform.zip

#Config Authentic Gitlab and AWS
export aws_access_key=${AWS_ACCESS_KEY_ID}                  
export aws_secret_access_key=${AWS_SECRET_ACCESS_KEY}
export aws_region=${AWS_DEFAULT_REGION}

#terraform init
terraform init

#validate terraform
terraform validate

#plan apply and destroy
terraform plan -lock=false -var aws_access_key=${AWS_ACCESS_KEY_ID} -var aws_secret_access_key=${AWS_SECRET_ACCESS_KEY} -var  aws_region=${AWS_DEFAULT_REGION}
terraform apply -auto-approve -lock=false -var aws_access_key=${AWS_ACCESS_KEY_ID} -var aws_secret_access_key=${AWS_SECRET_ACCESS_KEY} -var  aws_region=${AWS_DEFAULT_REGION}
#terraform destroy -auto-approve -lock=false -var aws_access_key=${AWS_ACCESS_KEY_ID} -var aws_secret_access_key=${AWS_SECRET_ACCESS_KEY} -var  aws_region=${AWS_DEFAULT_REGION}
