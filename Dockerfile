#FROM python:3.9
#Latest version of Mkdocs...mas moderna la vista
FROM squidfunk/mkdocs-material:9        

                #Esto ultimo es el repo desde donde instalare
RUN pip install mkdocs mkdocs material mkdocs-git-revision-date-localized-plugin

#Donde guardare el Contenido de la image builded...si no existiera lo creara con MKDIR
WORKDIR /docs           

#Copying the image builded to this folder
COPY . /docs

#Port del Container
EXPOSE 8000             
                                    #localhost
#CMD ["mkdocs", "serve", "--dev-addr=0.0.0.0:8000"] 
CMD ["serve", "--dev-addr=0.0.0.0:8000"] 